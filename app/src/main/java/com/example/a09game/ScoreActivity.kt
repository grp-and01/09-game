package com.example.a09game

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_score.*

class ScoreActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)

        val existingResults = Hawk.get(Constant.RESULTS, mutableListOf<Result>()).sortedByDescending { it.score }
        score_list.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1,
            existingResults.map { "${it.score} \t ${it.createdAt} \t ${it.name}" } )
    }
}
