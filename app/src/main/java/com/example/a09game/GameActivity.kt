package com.example.a09game

import android.graphics.Color
import android.graphics.Point
import android.os.Bundle
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.doOnPreDraw
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_game.*
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.random.Random
import kotlin.random.Random.Default.nextInt

class GameActivity : AppCompatActivity() {

    private var radius = 80
    private var level = 0
    private lateinit var searchingCircle : CircleView

    private val handler = Handler()
    private var canClick = AtomicBoolean(true)

    private val formatter = SimpleDateFormat("dd. MM. yyyy HH:mm", Locale("cs"))

    private val disableClicking = Runnable {
        canClick.set(false)
        if (Hawk.get(Constant.MAX_VALUE, 0) < level) {
            Hawk.put(Constant.MAX_VALUE, level)
            Hawk.put(Constant.MAX_TIME, Calendar.getInstance().time)
        }

        val date = Hawk.get<Date>(Constant.MAX_TIME, Calendar.getInstance().time)
        val currentDate = formatter.format(date)

        val existingResults = Hawk.get(Constant.RESULTS, mutableListOf<Result>())
        existingResults.add(Result(level, currentDate, intent.getStringExtra("name")))
        Hawk.put(Constant.RESULTS, existingResults)

        AlertDialog.Builder(this)
            .setCancelable(false)
            .setMessage("Reached level is $level")
            .setPositiveButton("Ok") { _, _ -> finish() }
            .show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        window.decorView.apply {
            systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        }

        searchingCircle = CircleView(this)
        searchingCircle.circle = Circle(Point(100, 150), radius)
        whole_layout.addView(searchingCircle)

        whole_layout.setOnTouchListener { v: View, event: MotionEvent ->

            if (event.action == MotionEvent.ACTION_DOWN) {
                onTouch(event)
            }

            return@setOnTouchListener true
        }

        whole_layout.doOnPreDraw { generateNewCircle() }
    }

    private fun generateNewCircle() {
        radius -= radius / 20
        val diameter = 2 * radius
        val x = Random.nextInt(diameter, whole_layout.width - radius)
        val y = Random.nextInt(diameter, whole_layout.height - radius)
        val randomColor = Color.argb(255, nextInt(250), nextInt(250), nextInt(250))
        searchingCircle.circle = Circle(Point(x, y), radius)
        searchingCircle.changeColor(randomColor)
    }

    private fun onTouch(event: MotionEvent) {
        if (canClick.get() && searchingCircle.circle.isIntersecting(event.x, event.y)) {
            level += 1
            welcome_text.text = "$level"
            generateNewCircle()
            searchingCircle.invalidate()

            handler.removeCallbacks(disableClicking)
            handler.postDelayed(disableClicking, 1000)
        }
    }
}
