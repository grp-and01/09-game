package com.example.a09game

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.orhanobut.hawk.Hawk
import kotlinx.android.synthetic.main.activity_main_menu.*


class MainMenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        Hawk.init(this).build()
        start_button.setOnClickListener {
            startActivity(Intent(this, GameActivity::class.java).apply { putExtra("name", edit_name.text.toString()) })
        }
        score_button.setOnClickListener {
            startActivity(Intent(this, ScoreActivity::class.java))
        }
    }

    override fun onStart() {
        super.onStart()
        val maxValue = Hawk.get<Int>(Constant.MAX_VALUE, 0)
        max_value.text = "$maxValue"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.deleteDb -> {
                Hawk.deleteAll()
            }
            R.id.finish -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
