package com.example.a09game

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Point
import android.util.AttributeSet
import android.view.View
import kotlin.math.sqrt

data class Circle(val center: Point, val radius: Int) {

    fun isIntersecting(x: Float, y: Float): Boolean {
        val diffX = x - center.x
        val diffY = y - center.y
        return sqrt(diffX * diffX + diffY * diffY) <= radius
    }

}

class CircleView
@JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {

    lateinit var circle: Circle

    private val paint = Paint().apply {
        style = Paint.Style.FILL
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawCircle(
            circle.center.x.toFloat(),
            circle.center.y.toFloat(),
            circle.radius.toFloat(),
            paint
        )
    }

    fun changeColor(color : Int)  {
        paint.color = color
    }
}