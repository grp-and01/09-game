package com.example.a09game

data class Result(val score: Int, val createdAt: String, val name: String)