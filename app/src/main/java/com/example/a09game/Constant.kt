package com.example.a09game

object Constant {
    const val MAX_VALUE = "max"
    const val MAX_TIME = "max_time"
    const val RESULTS = "results"
}